<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_MOD_PAGE')) {
    exit('Stop!!!');
}

if($nv_Request->isset_request('create', 'post,get')){
    $sql = "INSERT INTO nv4_vi_duanmoi_client(ho_va_ten, email, mat_khau, so_dien_thoai, created_at, updated_at) VALUES (:ho_va_ten,:email,:mat_khau,:so_dien_thoai,:created_at,:updated_at)";
    $stmt = $db->prepare($sql);
    $stmt->bindParam('ho_va_ten', $_POST['ho_va_ten']);
    $stmt->bindParam('email', $_POST['email']);
    $stmt->bindParam('mat_khau', $_POST['mat_khau']);
    $stmt->bindParam('so_dien_thoai', $_POST['so_dien_thoai']);
    $stmt->bindValue('created_at', NV_CURRENTTIME);
    $stmt->bindValue('updated_at', 0);
    $exe = $stmt->execute();
    if($exe){
        die('success');
    }
}

$xtpl = new XTemplate('dangky.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
$xtpl->parse('main');
$contents = $xtpl->text('main');
include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
